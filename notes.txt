package com.allstate.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmployeEntityTest {

    private Employee employee;
    private LocalDate localDate;
    @BeforeEach
    void setUp() {
        localDate = LocalDate.now();
        employee = new Employee(1,"Dee", 50,"d@i.ie", 1234.00);
    }

    @Test
    void getId() {
        assertEquals(1, employee.getId());
    }


    @Test
    void getName() {
        assertEquals("Dee", employee.getName());
    }

    @Test
    void getSalary() {
        assertEquals(150.00, employee.getSalary());
    }

    @Test
    void getEmail() { assertEquals(1234, employee.getEmail());
    }


}

//dao IT test
package com.allstate.dao;

import com.allstate.entities.Employee;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
//@ExtendWith(SpringExtension.class)
public class EmployeeDaoTest {

    @Autowired
    private EmployeeRepo dao;

    @Test
    public void save_And_findById_Success() {
        Employee employee = new Employee(1232,"Dee", 50,"d@i.ie", 1234.00);

        dao.save(employee);

        assertEquals(employee.getId(), dao.find(1232).getId());
    }


}


//Mocks
package com.allstate.dao;

import com.allstate.entities.Employee;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doNothing;
import static org.junit.jupiter.api.Assertions.assertEquals;

//@SpringBootTest
//@ExtendWith(SpringExtension.class)
public class EmployeeDaoMockTest {

    @Mock
    private EmployeeRepo dao;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }



    @Test
    void rowCount_success() {
        doReturn(1L).when(dao).count();
        assertEquals(1L, dao.count());
    }
    @Test
    public void save_And_findById_Success() {
        Employee employee = new Employee(1,"Dee", 50,"d@i.ie", 1234.00);
        doNothing().when(dao).save(any(Employee.class));

      //  assertEquals(employee, dao.save(new Employee(1,"Dee", 50,"d@i.ie", 1234.00)));


    }


}






